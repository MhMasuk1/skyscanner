# Skyscanner API

## Setup

The first thing to do is to clone the repository:

```sh
$ git clone https://gitlab.com/MhMasuk1/skyscanner.git
```

Build and up the docker:

```sh
$ docker-compose up --build
```

Now go to documentation of the API:

```sh
$ http://127.0.0.1:8000/docs
```