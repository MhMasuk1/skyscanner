import requests
from requests.exceptions import HTTPError

def get_community_api(location):
    """Weather information from community"""
    
    try:
        url = "https://community-open-weather-map.p.rapidapi.com/weather"
        
        querystring = {"q": location,"lat":"0","lon":"0","callback":"test","id":"2172797","lang":"null","units":"imperial","mode":"xml"}


        response = requests.get(
            url,
            headers = {
                'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com",
                'x-rapidapi-key': "e28136e4damsh4b908d20f9bfb37p1cb260jsn7d0ed0b8f81d"
            },
            params=querystring
        )

        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except HTTPError as http_err:
        print(f'get_community_api(): HTTP error occurred: {http_err}')
    except Exception as err:
        print(f'get_community_api(): Other error occurred: {err}')
    else:
        print('get_community_api(): Success!')

    return response


def get_weather_api(location):
    """Weather information from weather API"""
    
    try:
        url = "https://weatherapi-com.p.rapidapi.com/current.json"
        
        querystring = {"q": location}

        response = requests.get(
            url,
            headers = {
                'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
                'x-rapidapi-key': "e28136e4damsh4b908d20f9bfb37p1cb260jsn7d0ed0b8f81d"
            },
            params=querystring
        )

        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except HTTPError as http_err:
        print(f'get_weather_api(): HTTP error occurred: {http_err}')
    except Exception as err:
        print(f'get_weather_api(): Other error occurred: {err}')
    else:
        print('get_weather_api(): Success!')

    return response


def get_forcast_api(location):
    """Weather information from forcast"""
    
    try:
        url = f"https://forecast9.p.rapidapi.com/rapidapi/forecast/{location}/summary/"

        response = requests.get(
            url,
            headers = {
                'x-rapidapi-host': "forecast9.p.rapidapi.com",
                'x-rapidapi-key': "e28136e4damsh4b908d20f9bfb37p1cb260jsn7d0ed0b8f81d"
            },
        )

        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except HTTPError as http_err:
        print(f'get_forcast_api(): HTTP error occurred: {http_err}')
    except Exception as err:
        print(f'get_forcast_api(): Other error occurred: {err}')
    else:
        print('get_forcast_api(): Success!')

    return response