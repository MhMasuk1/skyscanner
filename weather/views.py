from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response

from utils import get_community_api, get_weather_api, get_forcast_api


class WeatherProviders(APIView):

    def get(self, request, location, format=None):
        """
        Return a list of all wether providers.
        """
        get_weather_api_data = get_weather_api("london")
        get_forcast_api_data = get_forcast_api(location)

        context = [
            {
                "provieder": "Weather Api",
                "data": get_weather_api_data.json()
            },
            {
                "provieder": "Forcast Api",
                "data": get_forcast_api_data.json()
            }
        ]
        
        return Response(context)




