from django.urls import path

from . import views

app_name = 'weather'

urlpatterns = [
    path('weather-providers/<location>', views.WeatherProviders.as_view()),
]